import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  exform!: FormGroup;

  constructor(private formBuilder: FormBuilder) {
  }
  
  ngOnInit(): void {
    this.exform = this.formBuilder.group({
      'ident' : new FormControl('intern1', Validators.required),
      'firstName' : new FormControl('Meriem'),
      'lastName' : new FormControl('Ahizoun'),
      'email' : new FormControl('xxx.yyy@gmail.com')
      
    });
  }

}

